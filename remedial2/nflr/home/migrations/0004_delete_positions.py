# Generated by Django 5.0 on 2023-12-05 17:00

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0003_positions_alter_players_position'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Positions',
        ),
    ]
