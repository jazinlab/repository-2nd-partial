from django.shortcuts import get_object_or_404, render, redirect
from .models import Stadiums, Teams, Players
from .forms import StadiumForm, TeamForm, PlayerForm

def home(request):
    return render(request, 'generaladmin.html')

#CRUDS STADIUMS
def stadium_list(request):
    stadiums = Stadiums.objects.all()
    return render(request, 'stadium_list.html', {'stadiums': stadiums})

def add_stadium(request):
    if request.method == 'POST':
        form = StadiumForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('stadium_list')  # Redirige a la lista de estadios después de agregar uno nuevo
    else:
        form = StadiumForm()
    
    return render(request, 'add_stadium.html', {'form': form})

def update_stadium(request, stadium_id):
    stadium = get_object_or_404(Stadiums, pk=stadium_id)
    
    if request.method == 'POST':
        form = StadiumForm(request.POST, instance=stadium)
        if form.is_valid():
            form.save()
            return redirect('stadium_list')  # Redirige a la lista de estadios después de actualizar uno existente
    else:
        form = StadiumForm(instance=stadium)
    
    return render(request, 'update_stadium.html', {'form': form, 'stadium': stadium})

def delete_stadium(request, stadium_id):
    stadium = get_object_or_404(Stadiums, pk=stadium_id)
    
    if request.method == 'POST':
        stadium.delete()
        return redirect('stadium_list')  # Redirige a la lista de estadios después de eliminar uno existente
    
    return render(request, 'delete_stadium.html', {'stadium': stadium})

#CRUDS TEAMS
def team_list(request):
    teams = Teams.objects.all()
    return render(request, 'team_list.html', {'teams': teams})

def add_team(request):
    if request.method == 'POST':
        form = TeamForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('team_list')  # Redirige a la lista de equipos después de agregar uno nuevo
    else:
        form = TeamForm()
    
    return render(request, 'add_team.html', {'form': form})

def update_team(request, team_id):
    team = get_object_or_404(Teams, pk=team_id)
    
    if request.method == 'POST':
        form = TeamForm(request.POST, instance=team)
        if form.is_valid():
            form.save()
            return redirect('team_list')  # Redirige a la lista de equipos después de actualizar uno existente
    else:
        form = TeamForm(instance=team)
    
    return render(request, 'update_team.html', {'form': form, 'team': team})

def delete_team(request, team_id):
    team = get_object_or_404(Teams, pk=team_id)
    
    if request.method == 'POST':
        team.delete()
        return redirect('team_list')  # Redirige a la lista de equipos después de eliminar uno existente
    
    return render(request, 'delete_team.html', {'team': team})

#CRUD PLAYERS
def player_list(request):
    players = Players.objects.all()
    return render(request, 'player_list.html', {'players': players})

def add_player(request):
    if request.method == 'POST':
        form = PlayerForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('player_list')  # Redirige a la lista de jugadores después de agregar uno nuevo
    else:
        form = PlayerForm()
    
    return render(request, 'add_player.html', {'form': form})

def update_player(request, player_id):
    player = get_object_or_404(Players, pk=player_id)
    
    if request.method == 'POST':
        form = PlayerForm(request.POST, instance=player)
        if form.is_valid():
            form.save()
            return redirect('player_list')  # Redirige a la lista de jugadores después de actualizar uno existente
    else:
        form = PlayerForm(instance=player)
    
    return render(request, 'update_player.html', {'form': form, 'player': player})

def delete_player(request, player_id):
    player = get_object_or_404(Players, pk=player_id)
    
    if request.method == 'POST':
        player.delete()
        return redirect('player_list')  # Redirige a la lista de jugadores después de eliminar uno existente
    
    return render(request, 'delete_player.html', {'player': player})

#Querys
def owner_list(request):
    owners = Teams.objects.values_list('ownername', flat=True).distinct()
    return render(request, 'owner_list.html', {'owners': owners})

def all_players_list(request):
    players = Players.objects.all()
    return render(request, 'all_players_list.html', {'players': players})

def team_players_list(request, team_id):
    team = Teams.objects.get(pk=team_id)
    players = Players.objects.filter(teams=team)
    return render(request, 'team_players_list.html', {'team': team, 'players': players})

def team_players_details(request, team_id):
    team = Teams.objects.get(pk=team_id)
    players = Players.objects.filter(teams=team)
    return render(request, 'team_players_details.html', {'team': team, 'players': players})


