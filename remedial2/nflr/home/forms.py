from django import forms
from .models import Stadiums, Teams, Players

class StadiumForm(forms.ModelForm):
    class Meta:
        model = Stadiums
        fields = ['name', 'capacity', 'boxsize']

class TeamForm(forms.ModelForm):
    class Meta:
        model = Teams
        fields = ['name', 'venues', 'ownername', 'stadium']

class PlayerForm(forms.ModelForm):
    class Meta:
        model = Players
        fields = ['name', 'playernum', 'teams', 'position']
