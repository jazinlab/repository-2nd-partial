from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('stadiums/', views.stadium_list, name='stadium_list'),
    path('teams/', views.team_list, name='team_list'),
    path('players/', views.player_list, name='player_list'),
    #STADIUM CRUDS
    path('stadiums/add/', views.add_stadium, name='add_stadium'),
    path('stadiums/<int:stadium_id>/update/', views.update_stadium, name='update_stadium'),
    path('stadiums/<int:stadium_id>/delete/', views.delete_stadium, name='delete_stadium'),
    #TEAMS CRUDS
    path('teams/add/', views.add_team, name='add_team'),
    path('teams/<int:team_id>/update/', views.update_team, name='update_team'),
    path('teams/<int:team_id>/delete/', views.delete_team, name='delete_team'),
    #PLAYERS CRUDS
    path('players/add/', views.add_player, name='add_player'),
    path('players/<int:player_id>/update/', views.update_player, name='update_player'),
    path('players/<int:player_id>/delete/', views.delete_player, name='delete_player'),
    #QUERYS
    path('owners/', views.owner_list, name='owner_list'),
    path('all_players/', views.all_players_list, name='all_players_list'),
    path('teams/<int:team_id>/players/', views.team_players_list, name='team_players_list'),
    path('teams/<int:team_id>/players/details/', views.team_players_details, name='team_players_details'),
]
