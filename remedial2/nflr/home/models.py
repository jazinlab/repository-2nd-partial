from django.db import models

# Create your models here.
class Stadiums(models.Model):
    name=models.CharField(max_length=50)
    capacity=models.IntegerField(default=0)
    boxsize=models.IntegerField(default=0)

    class Meta:
        db_table='Stadiums'
    def __str__(self):
        return self.name
    
class Teams(models.Model):
    name=models.CharField(max_length=25)
    venues=models.CharField(max_length=5)
    ownername=models.CharField(max_length=50)
    stadium=models.OneToOneField(Stadiums, on_delete=models.CASCADE)
    class Meta:
        db_table='Teams'
    def __str__(self):
        return self.name

class Players(models.Model):
    name=models.CharField(max_length=50)
    playernum=models.IntegerField(default=0)
    teams=models.OneToOneField(Teams, on_delete=models.CASCADE)
    position=models.CharField(max_length=5)
    class Meta:
        db_table='Players'
    def __str__(self):
        return self.name